from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
    remember,
    forget,
    )
from pyramid.view import (
    forbidden_view_config,
    view_config,
)

from math import pi
import shlex, subprocess
from ..models import Part
from ..models import Part_inventory

import logging
log = logging.getLogger(__name__)


@view_config(route_name='search_barcode', renderer='../templates/searchbarcode.jinja2')
def search_barcode(request):
# add po to database if not exist - see default.py add_page and view_page
# NOT adding for now. just verifying PO existence.
    message = ''
    if 'form.submitted' in request.params:
        barcodepn = request.params['barcodepn']
        barcodepn = barcodepn.upper()
        exists = request.dbsession.query(Part).filter_by(barcode_pn=barcodepn).all()
        if exists:
            next_url = request.route_url('create_carrier', barcodepn=barcodepn)
            return HTTPFound(location=next_url)
        else:
            return dict(
                url=request.route_url('search_barcode'),
                barcodepn = '',
                message = 'Not Found ' + str(barcodepn)
            )
    return dict(
        url=request.route_url('search_barcode'),
        )



@view_config(route_name='create_carrier', renderer='../templates/createcarrier.jinja2',
            permission='create')
def create_carrier(request):
#update carrier? if exists, update carrier, else create
    message = ""
    barcodepn = request.context.barcodepn
    log.debug("Request context")
    log.debug(str(request.context))
    if 'form.calculate' in request.params:
        lengthraw = request.params['length']
        length = lengthraw
        if request.params['holes']:
            holes = request.params['holes']
        else:
            holes = ""
        margin = request.params['margin']
        if request.params['thickness']:
            thickness = request.params['thickness']
        else:
            thickness = ""
        inradius = request.params['inradius']
        outradius = request.params['outradius']
        depot = request.params['depot']
        #if guessed in request.params : guessed = 1 else : guessed = 0
#        guessed = request.params['guessed']
        #log.debug(request.params)
        try:
            if length == "":
                log.debug("Found length")
                outradius = float(outradius)
                inradius = float(inradius)
                thickness = float(thickness)
            # Calculate length of tape if not provided
                numloops = ((outradius * 2) - (inradius * 2)) / (2 * thickness)
                length = pi * numloops * ((inradius * 2) + (thickness * (numloops - 1)))
            # Calculate number of parts by raw length of tape
            log.debug("calc quantity")
            length = float(length)
            holes = float(holes)
            margin = float(margin)
            quantity = length / (holes * 4.0)
            #reduce by safety margin.
            quantity = quantity - (margin / 100.0)
            quantity = int(quantity)
            log.debug("return dict")
            return dict(
                url=request.route_url('create_carrier', barcodepn = barcodepn),
                barcodepn = barcodepn,
                quantity = quantity,
                holes = holes,
                length = lengthraw,
                margin=margin,
                inradius=inradius,
                outradius=outradius,
                thickness=thickness,
                depot=depot,
                guessed = "checked",
                message = 'Quantity calculated'
                )
        except:
            return dict(
                url=request.route_url('create_carrier', barcodepn = barcodepn),
                barcodepn = barcodepn,
                holes = holes,
                length = lengthraw,
                margin=margin,
                inradius=inradius,
                outradius=outradius,
                thickness=thickness,
                depot=depot,
                message = 'Math problem!!!'
                )
            
    if 'form.submitted' in request.params:
        barcodepn = request.params['barcodepn']
        barcodepn = barcodepn.upper()
        quantityraw = request.params['quantity']
        quantity = quantityraw.upper()
        quantity = quantity.strip("Q")
        thickness = request.params['thickness']
        holes = request.params['holes']
        depot = request.params['depot']
        lengthraw = request.params['length']
        margin = request.params['margin']
        inradius = request.params['inradius']
        outradius = request.params['outradius']
        depot = request.params['depot']
        guessed = bool("1") if "guessed" in request.params.keys() else bool("")
        try:
            int(quantity)
        except:
            return dict(
                url=request.route_url('create_carrier', barcodepn = barcodepn),
                barcodepn = barcodepn,
                quantity = quantityraw,
                length = lengthraw,
                margin=margin,
                inradius=inradius,
                outradius=outradius,
                thickness=thickness,
                guessed = guessed,
                depot=depot,
                message = 'Quantity "' + str(quantityraw) + '" not integer'
                )
        # Assign database values
        if request.dbsession.query(Part).filter_by(barcode_pn=barcodepn).count() > 0:
            part = request.dbsession.query(Part).filter_by(barcode_pn=barcodepn).one()
            carrier = Part_inventory(part_number=part.part_number)
            carrier.barcode_pn=barcodepn
            carrier.part_id = part.id
            carrier.stock = quantity
            carrier.depot = "Tower"
            carrier.guessed = guessed
            carrier.stock_new = quantity
#            carrier.supply = 'R'
            carrier.inv_creator = request.user
            try:
                if float(thickness):
                    part.tape_thickness = thickness
                log.debug(str(holes))
                if float(holes):
                    steplength = holes * 4.0
                    part.tape_step = int(steplength)
            except:
                log.debug("No thickness or holes?")
            if depot != "":
                carrier.depot = depot
            request.dbsession.add(carrier)
            request.dbsession.flush()
            #Commit line to database
#carrier added to database here. inventory wrong if error and new id created.
            request.dbsession.refresh(carrier)
            add_carrier_file(carrier)
            print_barcode(carrier)
            next_url = request.route_url('search_barcode')
            message = 'Added ' + str(carrier.stock) + ' pieces ' + str(carrier.carrier) + ' carrier '+ str(carrier.part_number)
            return HTTPFound(location=next_url)
#should send back to search page, but does not?
#should just send to loading page until complete?
        else: #part number not found
            return dict(
                url=request.route_url('search_barcode'),
#                ponumber = ponumber,
                message = 'Not Found ' + str(barcodepn)
                )
    # Default view
    if request.dbsession.query(Part).filter_by(barcode_pn=barcodepn).count() > 0:
        part = request.dbsession.query(Part).filter_by(barcode_pn=barcodepn).one()
        message="Part Number " + str(part.part_number)
        log.debug("Default view " + str(part.part_number))
        if part.tape_thickness:
            thickness = part.tape_thickness
        else:
            thickness = ""
        if part.tape_step:
            step = part.tape_step
            holes = float(step) / 4
        else:
            holes = ""

    else:
        thickness = ""
        holes = ""
        message = "Part not found " + str(barcodepn)
        log.debug(message)
    return dict(
        barcodepn = barcodepn,
        message=message,
        margin=15,
        thickness=thickness,
        holes=holes,
        guessed="",
        depot=""
        )

@view_config(route_name='recent_carriers', renderer='../templates/recentcarriers.jinja2')
def recent_carriers(request):
    recent = request.dbsession.query(Part_inventory).\
        order_by(Part_inventory.created.desc()).limit(5).all()
    return dict(recent=recent)

@view_config(route_name='reprint_carrier', renderer='../templates/reprint.jinja2')
def reprint_carriers(request):
    printcmd = shlex.split("lp output.pdf")
    subprocess.Popen(printcmd).wait()
    message = 'Complete'
    return dict(message=message)

def add_carrier_file(carrier):
    filename = "addcarrier" + str(carrier.carrier) + ".ETO"
    file = open(filename,"w")
    file.write("[Order]\n")
    file.write("Action=NEW\n")
    file.write("Object=CARRIER\n")
    file.write("Name=" + str(carrier.carrier)+"\n\n")
    file.write("[Data]\n")
    file.write("ARTICLE=" + str(carrier.part_number) + "\n")
    file.write("STOCK=" + str(carrier.stock) + "\n")
    file.close()

    samba_upload_cmd = shlex.split('smbclient //192.168.1.201/Orders -N -c "put ' + filename + '"')
    subprocess.Popen(samba_upload_cmd).wait()

def update_carrier_file(carrier):
    filename = "updatecarrier" + str(carrier.carrier) + ".ETO"
    file = open(filename,"w")
    file.write("[Order]\n")
    file.write("Action=UPDATE\n")
    file.write("Object=CARRIER\n")
    file.write("Name=" + str(carrier.carrier)+"\n\n")
    file.write("[Data]\n")
    file.write("ARTICLE=" + str(carrier.part_number) + "\n")
    file.write("STOCK=" + str(carrier.stock) + "\n")
    file.close()

    samba_upload_cmd = shlex.split('smbclient //192.168.1.201/Orders -N -c "put ' + filename + '"')
    subprocess.Popen(samba_upload_cmd).wait()

def print_barcode(carrier):
#depends on configurations in the glabels file
#merge file location critical...
    filename = "barcode.txt"
    file = open(filename, "w")
    file.write('"' + str(carrier.part_number) +'",')
    file.write('"P' + str(carrier.part_number) +'",')
    file.write(str(carrier.carrier)+",")
    file.write("R" + str(carrier.carrier)+",")
    file.write(str(carrier.stock)+",")
    file.write("Q" + str(carrier.stock)+"\n")
    file.close()

    glabelcmd = shlex.split("glabels-3-batch towerlabel.glabels")
    printcmd = shlex.split("lp output.pdf")
    subprocess.Popen(glabelcmd).wait()

    subprocess.Popen(printcmd).wait()

@view_config(route_name='search_carrier', renderer='../templates/searchcarrier.jinja2')
def search_carrier(request):
# add po to database if not exist - see default.py add_page and view_page
# NOT adding for now. just verifying PO existence.
#    if request.matchdict['message']:
    message = request.matchdict['message']
    log.debug("DEBUG MSG - " + str(message))
#    else:
#        message = ''
    if 'form.submitted' in request.params:
        carrierid = request.params['carrierid']
        carrierid = carrierid.upper()
        carrierid = carrierid.strip("R")
        try:
            int(carrierid)
        except:
            message = 'Invalid ID ' + str(carrierid)
            return dict(
                url=request.route_url('search_carrier', message=message),
                message = message,
                carrierid = ''
            )
        exists = request.dbsession.query(Part_inventory).filter_by(carrier=carrierid).all()
        if exists:
            next_url = request.route_url('update_carrier', carrierid=carrierid)
            return HTTPFound(location=next_url)
        else:
            message = 'Not Found ' + str(carrierid)
            return dict(
                url=request.route_url('search_carrier', message=message),
                message=message,
                carrierid = ''
            )
    return dict(
        url=request.route_url('search_carrier', message=message),
        message=message
        )

   

@view_config(route_name='update_carrier', renderer='../templates/updatecarrier.jinja2',
            permission='edit')
def update_carrier(request):
#update carrier? if exists, update carrier, else create
    message = "Carrier not found"
    thickness = ""
    holes = ""
    carrierid = request.context.carrierid
    log.debug(str(carrierid))
    if request.dbsession.query(Part_inventory).filter_by(carrier=carrierid).count() > 0:
        log.debug("id found")
        carrier = request.dbsession.query(Part_inventory).filter_by(carrier=carrierid).one()
        part = request.dbsession.query(Part).filter_by(id=carrier.part_id).one()
        carrierid = carrier.carrier
        if 'form.calculate' in request.params:
            lengthraw = request.params['length']
            holes = request.params['holes']
            margin = request.params['margin']
            thickness = request.params['thickness']
            inradius = request.params['inradius']
            outradius = request.params['outradius']
            length = lengthraw
            holes = request.params['holes']
            margin = request.params['margin']
            thickness = request.params['thickness']
            inradius = request.params['inradius']
            outradius = request.params['outradius']
            depot = request.params['depot']
            #if guessed in request.params : guessed = 1 else : guessed = 0
#            guessed = request.params['guessed']
            log.debug(request.params)
            try:
                if length == "":
                    log.debug("Found length")
                    outradius = float(outradius)
                    inradius = float(inradius)
                    thickness = float(thickness)
                # Calculate length of tape if not provided
                    numloops = ((outradius * 2) - (inradius * 2)) / (2 * thickness)
                    length = pi * numloops * ((inradius * 2) + (thickness * (numloops - 1)))
                # Calculate number of parts by raw length of tape
                log.debug("calc quantity")
                length = float(length)
                holes = float(holes)
                margin = float(margin)
                quantity = length / (holes * 4.0)
                #reduce by safety margin.
                quantity = quantity - (margin / 100.0)
                quantity = int(quantity)
                log.debug("return dict")
                return dict(
                    url=request.route_url('update_carrier', carrierid=carrierid),
                    quantity = quantity,
                    carrierid = carrierid,
                    holes = holes,
                    length = lengthraw,
                    margin=margin,
                    inradius=inradius,
                    outradius=outradius,
                    thickness=thickness,
                    depot=depot,
                    guessed = "checked",
                    message = 'Quantity calculated Part # ' + part.part_number
                    )
            except:
                return dict(
                    url=request.route_url('update_carrier', carrierid=carrierid),
                    holes = holes,
                    length = lengthraw,
                    margin=margin,
                    carrierid = carrierid,
                    inradius=inradius,
                    outradius=outradius,
                    thickness=thickness,
                    depot=depot,
                    message = 'Math problem!!!'
                    )
            
        if ('form.submitted' in request.params) or ('form.print' in request.params):
            lengthraw = request.params['length']
            holes = request.params['holes']
            margin = request.params['margin']
            thickness = request.params['thickness']
            inradius = request.params['inradius']
            outradius = request.params['outradius']
            log.debug("form submitted")
            quantityraw = request.params['quantity']
            quantity = quantityraw.upper()
            quantity = quantity.strip("Q")
            thickness = request.params['thickness']
            depot = request.params['depot']
            holes = request.params['holes']
            guessed = bool("1") if "guessed" in request.params.keys() else bool("")
#            log.debug("parse carrier field")
#            carrierid = request.params['carrierid']
#            carrierid = carrierid.upper()
#            carrierid = carrierid.strip("R")
#            try:
#                int(carrierid)
#            except:
#                return dict(
#                    url=request.route_url('search_carrier'),
#                    message = 'Invalid ID ' + str(carrierid),
#                    carrierid = '',
#                    quantity = quantityraw,
#                    holes = holes,
#                    length = lengthraw,
#                    margin=margin,
#                    inradius=inradius,
#                    outradius=outradius,
#                    thickness=thickness,
#                    guessed = guessed
#                )
            log.debug("carrier " + str(carrierid))
            try:
                int(quantity)
            except:
                if 'form.print' in  request.params:
                    quantity = carrier.stock
                else:
                    return dict(
                        url=request.route_url('update_carrier', carrierid = carrierid),
                        carrierid = carrierid,
                        quantity = quantityraw,
                        holes = holes,
                        length = lengthraw,
                        margin=margin,
                        inradius=inradius,
                        outradius=outradius,
                        thickness=thickness,
                        guessed = guessed,
                        depot = depot,
                        message = 'Quantity "' + str(quantityraw) + '" not integer'
                        )
            # Assign database values
            carrier.stock = quantity
            carrier.guessed = guessed
#                carrier.supply = 'R'
            carrier.inv_creator = request.user
            try:
                thickness = float(thickness)
                part.tape_thickness = thickness
            except:
                log.debug("No thickness")
            log.debug(str(holes))
            try:
                holes = float(holes)
                steplength = holes * 4.0
                part.tape_step = int(steplength)
            except:
                log.debug("No holes")
            if carrier.depot:
                if depot != carrier.depot:
                    carrier.depot_old = carrier.depot
                    carrier.depot = depot
            else:
                if depot != "":
                    if depot != carrier.depot:
                        carrier.depot_old = carrier.depot
                        carrier.depot = depot
#                request.dbsession.add(carrier)
            request.dbsession.flush()
                #Commit line to database
#carrier added to database here. inventory wrong if error and new id created.
            request.dbsession.refresh(carrier)
            if carrier.active == True:
                update_carrier_file(carrier)                    
            else:
                add_carrier_file(carrier)
            if 'form.print' in request.params:
                log.debug("Print label")
                print_barcode(carrier)
#            next_url = request.route_url('search_carrier')
            message = 'Updated ' + str(carrier.stock) + ' pieces ' + str(carrier.carrier) + ' carrier '+ str(carrier.part_number)
            # Return for successfully updated carrier
#            return HTTPFound('location=search_carrier', message=message)
            next_url = request.route_url('search_carrier', message=message)
            return HTTPFound(location=next_url)
#            return dict(
#                url=request.route_url('search_carrier', message=message))
#                message=message)
        # Default view
        message="Carrier " + str(carrierid) + ", Part # " + str(part.part_number)\
            + ", Last count " + str(carrier.stock)
        
        if part.tape_thickness:
            thickness = part.tape_thickness
        else:
            thickness = ""
        if part.tape_step:
            step = part.tape_step
            holes = float(step) / 4
        else:
            holes = ""
        if carrier.depot:
            depot = carrier.depot
        else:
            depot = ""
        return dict(
            url=request.route_url('update_carrier', carrierid = carrierid),
            carrierid = carrierid,
            message=message,
            margin=15,
            thickness=thickness,
            holes=holes,
            guessed="",
            depot=depot
            )
    # If carrier id not found in database
    else:
        next_url = request.route_url('search_carrier', message=message)
        return HTTPFound(location=next_url)

