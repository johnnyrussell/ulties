from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
    remember,
    forget,
    )
from pyramid.view import (
    forbidden_view_config,
    view_config,
)

from ..models import Purchase_order
from ..models import Part
from ..models import Receive_line

@view_config(route_name='search_po', renderer='../templates/searchpo.jinja2')
def search_po(request):
# add po to database if not exist - see default.py add_page and view_page
# NOT adding for now. just verifying PO existence.
    message = ''
    if 'form.submitted' in request.params:
        ponumber = request.params['ponumber']
        exists = request.dbsession.query(Purchase_order).filter_by(po_number=ponumber).all()
        if exists:
            next_url = request.route_url('receive_part', ponumber=ponumber)
            return HTTPFound(location=next_url)
        else:
#            user = request.user
#            request.dbsession.add(Purchase_order)
#            next_url = request.route_url('create_po', ponumber=ponumber)
#            return HTTPFound(location=next_url)
            return dict(
                url=request.route_url('search_po'),
                ponumber = ponumber,
                message = 'Not Found ' + str(ponumber)
        )
    return dict(
        url=request.route_url('search_po'),
#        ponumber = ponumber,
        #message = 'Found ' + str(ponumber)
        )

#create_po
#@view_config(route_name='create_po', renderer='../templates/createpo.jinja2',
#            permission='create')
#def receivepo(request):
# add po to database if not exist - see default.py add_page and view_page
#    ponumber = request.params['ponumber']
#    if 'form.submitted' in request.params:
#        exists = request.dbsession.query(Purchase_order).filter_by(po_number=ponumber).all()
#        if exists:
#            next_url = request.route_url('receive_part', ponumber=ponumber)
#            return HTTPFound(location=next_url)
#        else:
#            user = request.user
#            request.dbsession.add(Purchase_order)
#            next_url = request.route_url('receive_part', ponumber=ponumber)
#            return HTTPFound(location=next_url)
#    return dict(
#        url=request.route_url('receive_po'),
#        ponumber=ponumber,
#        )

@view_config(route_name='receive_part', renderer='../templates/receivepart.jinja2',
            permission='create')
def receivepart(request):
    ponumber = request.context.ponumber
    message = 'PO Number ' + ponumber
    if 'form.submitted' in request.params:
        barcodepn = request.params['barcodepn']
        quantity = request.params['quantity']
        #Test barcode
        if request.dbsession.query(Part).filter_by(part_number=barcodepn).count() > 0:
            part = request.dbsession.query(Part).filter_by(part_number=barcodepn).one()
#            if part:
            line = Receive_line(barcode_pn=barcodepn, received_quantity=quantity)
            line.part_id = part.id
            line.part_number = part.part_number
            line.po_number = ponumber
            line.receive_creator = request.user
            #Commit line to database
            request.dbsession.add(line)
            next_url = request.route_url('receive_part', ponumber=ponumber)
            message = 'Checked in ' + str(quantity) + ' pieces '+ str(line.part_number)
        else: #part number not found
            return dict(
                url=request.route_url('receive_part', ponumber=ponumber),
                ponumber = ponumber,
                message = 'Not Found ' + str(barcodepn)
        )
    return dict(
        message=message,
        )


