from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
   remember,
   forget,
   )
from pyramid.view import (
   forbidden_view_config,
   view_config,
)
from ..models import Part
 
import logging
log = logging.getLogger(__name__)
 
@view_config(route_name='search_part_number', renderer='../templates/searchpartnumber.jinja2')
def search_part_number(request):
   message = ''
   if 'form.search' in request.params:
   # Show repeating rows for parts found
       partnumbersearch = request.params['partnumbersearch']
       parts = request.dbsession.query(Part).\
       filter(Part.part_number.ilike('%' + partnumbersearch + '%')).\
       order_by(Part.part_number).limit(500).all()
       if parts:
           foundpart = True
           return dict(
               url=request.route_url('search_part_number'),
               foundpart = foundpart,
               parts = parts
               )
       else:
           # Part not found. Display message and search again
           foundpart = False
           message = 'Part not found "' + str(partnumbersearch) + '"'
          
           return dict(
               url=request.route_url('search_part_number'),
               foundpart = foundpart,
               message = message
               )
          
   # Default view
   message = "Enter a part of the part number you are looking for."
   return dict(
       url=request.route_url('search_part_number'),
       foundpart = False,
       message = message
       )
  
  
# View for editing parts
@view_config(route_name='edit_part_data', renderer='../templates/editpartdata.jinja2',
           permission='edit')
def edit_part_data(request):
   message = ''
   partnumber = request.context.partnumber
   log.debug(str(partnumber))
 
   if request.dbsession.query(Part).filter_by(part_number=partnumber).count() > 0:
       part = request.dbsession.query(Part).filter_by(part_number=partnumber).one()
       if 'form.save' in request.params:
       #Save changes to data
           log.debug("Save")
           description = request.params['description']
           barcodepn = request.params['barcodepn']
           barcodepn = barcodepn.upper()
           barcodespn = request.params['barcodespn']
           barcodespn = barcodespn.upper()
           manufacturer = request.params['manufacturer']
           mpn = request.params['mpn']
           multiple = request.params['multiple']
           tapestep = request.params['tapestep']
           tapethickness = request.params['tapethickness']
           message = "Saved part data: "
           if description != "":
               if part.description != description:
                   part.description = description
                   message += "description = " + str(description) + ", "
           if barcodepn != "":
               if part.barcode_pn != barcodepn:
                   part.barcode_pn = barcodepn
                   message += "barcode_pn = " + str(barcodepn) + ", "
           if barcodespn != "":
               if part.barcode_spn != barcodespn:
                   part.barcode_spn = barcodespn
                   message += "barcode_spn = " + str(barcodespn) + ", "
           if manufacturer != "":
               if part.manufacturer != manufacturer:
                   part.manufacturer = manufacturer
                   message +=  "manufacturer = " + str(manufacturer) + ", "
           if mpn != "":
               if part.mpn != mpn:
                   part.mpn = mpn
                   message += "mpn = " + str(mpn) + ", "
           if multiple != "":
               if int(part.multiple) != int(multiple):
                   part.multiple = int(multiple)
                   message += "multiple = " + str(multiple) + ", "
           if tapestep != "":
               if part.tape_step != int(tapestep):
                   part.tape_step = int(tapestep)
                   message += "tapestep = " + str(tapestep) + ", "
           if tapethickness != "":
               if float(part.tape_thickness) != float(tapethickness):
                   part.tape_thickness = float(tapethickness)
                   message += "tapethickness = " + str(tapethickness)
           request.dbsession.flush()
           if part.description:
               description = part.description
           else:
               description = ""
           if part.barcode_pn:
               barcodepn = part.barcode_pn
           else:
               barcodepn = ""
           if part.barcode_spn:
               barcodespn = part.barcode_spn
           else:
               barcodespn = ""
           if part.manufacturer:
               manufacturer = part.manufacturer
           else:
               manufacturer = ""
           if part.mpn:
               mpn = part.mpn
           else:
               mpn = ""
           if part.multiple:
               multiple = part.multiple
           else:
               multiple = ""
           if part.tape_step:
               tapestep = part.tape_step
           else:
               tapestep = ""
           if part.tape_thickness:
               tapethickness = part.tape_thickness
           else:
               tapethickness = ""
  
           return dict(
               url=request.route_url('edit_part_data', partnumber=partnumber),
               message=message,
               partnumber=partnumber,
               description=description,
               barcodepn=barcodepn,
               barcodespn=barcodespn,
               manufacturer=manufacturer,
               mpn=mpn,
               multiple=multiple,
               tapestep=tapestep,
               tapethickness=tapethickness
               )
      
       # Populate form with part data
       log.debug("View")
       if part.description:
           description = part.description
       else:
           description = ""
       if part.barcode_pn:
           barcodepn = part.barcode_pn
       else:
           barcodepn = ""
       if part.barcode_spn:
           barcodespn = part.barcode_spn
       else:
           barcodespn = ""
       if part.manufacturer:
           manufacturer = part.manufacturer
       else:
           manufacturer = ""
       if part.mpn:
           mpn = part.mpn
       else:
           mpn = ""
       if part.multiple:
           multiple = part.multiple
       else:
           multiple = ""
       if part.tape_step:
           tapestep = part.tape_step
       else:
           tapestep = ""
       if part.tape_thickness:
           tapethickness = part.tape_thickness
       else:
           tapethickness = ""
      
      
       return dict(
           url=request.route_url('edit_part_data', partnumber=partnumber),
           message=message,
           partnumber=partnumber,
           description=description,
           barcodepn=barcodepn,
           barcodespn=barcodespn,
           manufacturer=manufacturer,
           mpn=mpn,
           multiple=multiple,
           tapestep=tapestep,
           tapethickness=tapethickness
           )
      
   # Default view
   message = "Part number not found."
   return dict(
       url=request.route_url('edit_part_data'),
       message=message
       )
 
 
# View for creating parts
@view_config(route_name='create_part_data', renderer='../templates/createpartdata.jinja2',
           permission='create')
def create_part_data(request):
   message = ""
   partnumber = request.context.partnumber

   log.debug("Request context")
   log.debug(str(request.context))
   if 'form.save' in request.params:
       log.debug("Save")
       description = request.params['description']
       partnumber = request.params['partnumber']
       barcodepn = request.params['barcodepn']
       barcodepn = barcodepn.upper()
       barcodespn = request.params['barcodespn']
       barcodespn = barcodespn.upper()
       manufacturer = request.params['manufacturer']
       mpn = request.params['mpn']
       multiple = request.params['multiple']
       tapestep = request.params['tapestep']
       tapethickness = request.params['tapethickness']
       message = "Saved part data: "
 
       part = Part(part_number=partnumber)
       part.part_number = partnumber

       if description == "":
         part.description = None
       else:
         part.description = description
       message += "description = " + str(description) + ", "

       part.barcode_pn = barcodepn
       message += "barcode_pn = " + str(barcodepn) + ", "
 
       if barcodespn == "":
         part.barcode_spn = None
       else:
         part.barcode_spn = barcodespn
       message += "barcode_spn = " + str(barcodespn) + ", "

       if manufacturer == "":
         part.manufacturer = None
       else:
         part.manufacturer = manufacturer
       message +=  "manufacturer = " + str(manufacturer) + ", "

       if mpn == "":
         part.mpn = None
       else:
         part.mpn = mpn
       message += "mpn = " + str(mpn) + ", "

       if multiple == "":
         part.multiple = None
       else:
         part.multiple = int(multiple)
       message += "multiple = " + str(multiple) + ", "

       if tapestep == "":
         part.tape_step = None
       else:
         part.tape_step = int(tapestep)
       message += "tapestep = " + str(tapestep) + ", "

       if tapethickness == "":
         part.tape_thickness = None
       else:
         part.tape_thickness = float(tapethickness)
       message += "tapethickness = " + str(tapethickness)
 
       request.dbsession.add(part)
       request.dbsession.flush()
 
       if part.description:
           description = part.description
       else:
           description = ""
       if part.barcode_pn:
           barcodepn = part.barcode_pn
       else:
           barcodepn = ""
       if part.barcode_spn:
           barcodespn = part.barcode_spn
       else:
           barcodespn = ""
       if part.manufacturer:
           manufacturer = part.manufacturer
       else:
           manufacturer = ""
       if part.mpn:
           mpn = part.mpn
       else:
           mpn = ""
       if part.multiple:
           multiple = part.multiple
       else:
           multiple = ""
       if part.tape_step:
           tapestep = part.tape_step
       else:
           tapestep = ""
       if part.tape_thickness:
           tapethickness = part.tape_thickness
       else:
           tapethickness = ""

       return dict(
           url=request.route_url('create_part_data', partnumber=partnumber),
           message=message,
           partnumber=partnumber,
           description=description,
           barcodepn=barcodepn,
           barcodespn=barcodespn,
           manufacturer=manufacturer,
           mpn=mpn,
           multiple=multiple,
           tapestep=tapestep,
           tapethickness=tapethickness
           )
      
   # Default view Populate form with part data
   log.debug("View")
   partnumber = request.context.partnumber
   if request.dbsession.query(Part).filter_by(part_number=partnumber).count() > 0:
      part = request.dbsession.query(Part).filter_by(part_number=partnumber).one()

      if part.part_number:
         partnumber = part.part_number
      else:
         partnumber =""
      if part.description:
         description = part.description
      else:
         description = ""
      if part.barcode_pn:
         barcodepn = part.barcode_pn
      else:
         barcodepn = ""
      if part.barcode_spn:
         barcodespn = part.barcode_spn
      else:
         barcodespn = ""
      if part.manufacturer:
         manufacturer = part.manufacturer
      else:
         manufacturer = ""
      if part.mpn:
         mpn = part.mpn
      else:
         mpn = ""
      if part.multiple:
         multiple = part.multiple
      else:
         multiple = ""
      if part.tape_step:
         tapestep = part.tape_step
      else:
         tapestep = ""
      if part.tape_thickness:
         tapethickness = part.tape_thickness
      else:
         tapethickness = ""

      return dict(
          url=request.route_url('create_part_data', partnumber=partnumber),
          message=message,
          partnumber=partnumber,
          description=description,
          barcodepn=barcodepn,
          barcodespn=barcodespn,
          manufacturer=manufacturer,
          mpn=mpn,
          multiple=multiple,
          tapestep=tapestep,
          tapethickness=tapethickness
          )
   message = "Add new part"
   return dict(
      )
