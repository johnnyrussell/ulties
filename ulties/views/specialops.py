from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
    remember,
    forget,
    )
from pyramid.view import (
    forbidden_view_config,
    view_config,
)

import shlex, subprocess
from ..models import Part_inventory
from ..models import Part_tower_carrier
from sqlalchemy import and_

import logging
log = logging.getLogger(__name__)


@view_config(route_name='special_operations', renderer='../templates/specialops.jinja2',
            permission='view')
def special_operations(request):
    message = ''
    if 'form.pulltowerdepots' in request.params:
        display = "confirmation"
        next_operation = "pulltowerdepots"
        message = "This will update a carriers depot to the depot from the tower if in tower. Must purge and refil temp database"
        return dict(
            display=display,
            message = message,
            next_operation=next_operation
            )
    if 'form.consumeparts' in request.params:
        display = "confirmation"
        next_operation = "consumeparts"
        message = "This is temporary patch to correct inventory. DO NOT RUN!"
        return dict(
            display=display,
            message = message,
            next_operation=next_operation
            )
    if ('form.confirm' in request.params):
        next_operation = request.params['next_operation']
        message = "Updated inventory depots that are stored in part tower "
        if next_operation == "pulltowerdepots":
            next_operation = "none"
            display = "none"
            for inv, car in request.dbsession.query(Part_inventory, Part_tower_carrier).\
                    filter(and_(Part_inventory.carrier == Part_tower_carrier.carrier),
                    (Part_tower_carrier.depot.ilike("Tower 001402%"))).order_by(Part_inventory.carrier).all():
                inv.depot = car.depot
                request.dbsession.add(inv)
                message = message + "," + str(inv.carrier)
            return dict(
                display=display,
                message=message,
                next_operation=next_operation
                )
        if next_operation == "consumeparts":
            next_operation = "none"
            display = "none"
            message = "DISABLED IN CODE: Zeroed inventory with no known location"
# Disabled until a safer alternative is developed
#            message = "Zeroed inventory with no known location"
#            for inv in request.dbsession.query(Part_inventory).\
#                    filter(Part_inventory.depot == None).order_by(Part_inventory.carrier).all():
#                inv.stock = 0
#                inv.active = False
#                request.dbsession.add(inv)
#                delete_carrier_file(inv.carrier)
#                message = message + "," + str(inv.carrier)
            return dict(
                display=display,
                message=message,
                next_operation=next_operation
                )

    if 'form.cancel' in request.params:
        message = "Cancelled operation"
    next_operation = "menu"
    display = "menu"
    return dict(
        display=display,
        message=message,
        next_operation=next_operation
        )



def delete_carrier_file(carrierid):
    filename = "deletecarrier" + str(carrierid) + ".ETO"
    file = open(filename,"w")
    file.write("[Order]\n")
    file.write("Action=DELETE\n")
    file.write("Object=CARRIER\n")
    file.write("Name=" + str(carrierid)+"\n")
    file.close()

    samba_upload_cmd = shlex.split('smbclient //192.168.1.201/Orders -N -c "put ' + filename + '"')
    subprocess.Popen(samba_upload_cmd).wait()

