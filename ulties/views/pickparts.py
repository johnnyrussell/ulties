from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
    remember,
    forget,
    )
from pyramid.view import (
    forbidden_view_config,
    view_config,
)
from ..models import Part
from ..models import Part_inventory
from sqlalchemy import and_
import shlex, subprocess
import logging
log = logging.getLogger(__name__)

@view_config(route_name='pick_parts', renderer='../templates/pickparts.jinja2')
def pick_parts(request):
    message = ''
    if 'partnumbersearch' in request.params:
    # Show repeating rows for parts found
        partnumbersearch = request.params['partnumbersearch']
        if len(partnumbersearch) > 1:
            if partnumbersearch[0] == 'P':
                partnumbersearch = partnumbersearch[1:]
        parts = request.dbsession.query(Part).\
        filter(Part.part_number.ilike('%' + partnumbersearch + '%')).\
        order_by(Part.part_number).limit(500).all()
        if parts:
            foundpart = True
            return dict(
                foundpart = foundpart,
                parts = parts
                )
        else:
            # Part not found. Display message and search again
            foundpart = False
            message = 'Part not found "' + str(partnumbersearch) + '"'
            
            return dict(
                foundpart = foundpart,
                message = message
                )
            
    # Default view
    message = "Enter a part of the part number you are looking for."
    return dict(
        foundpart = False,
        message = message
        )
    
    
# View for editing parts
@view_config(route_name='pick_depots', renderer='../templates/pickdepots.jinja2',
            permission='edit')
def pick_depots(request):
    message = ''
    partnumber = request.context.partnumber
    log.debug(str(partnumber))

    if 'form.search' in request.params:
        partnumbersearch = request.params['partnumbersearch']
        if partnumbersearch != "":
            partnumbersearch = partnumbersearch
        else:
            partnumbersearch = partnumber
    # Show repeating rows for parts found
        return dict(
            url=request.route_url('pick_parts'),
            partnumbersearch=partnumbersearch
            )
    if request.dbsession.query(Part).filter_by(part_number=partnumber).count() > 0:
        part = request.dbsession.query(Part).filter_by(part_number=partnumber).one()
        carriers = request.dbsession.query(Part_inventory)
        carriers = carriers.filter(and_(Part_inventory.part_id==part.id),
            (Part_inventory.active==True)).all()
        foundpart = True
        return dict(
            message=message,
            carriers=carriers,
            foundpart=foundpart
            )
        
    # Default view
    foundpart = False
    message = "Part number not found."
    carriers = ""
    return dict(
        message=message,
        partnumber=partnumber,
        carriers=carriers,
        foundpat=foundpart
        )


@view_config(route_name='provide_part', renderer='../templates/providepart.jinja2',
            permission='edit')
def provide_part(request):
    message = "Part provided by tower "
    carrierid = request.context.carrierid
    message = message + str(carrierid)
    if request.dbsession.query(Part_inventory).filter_by(carrier=carrierid).count() > 0:
        log.debug("id found")
        carrier = request.dbsession.query(Part_inventory).filter_by(carrier=carrierid).one()
        if carrier.depot[:5] == "Tower":
            provide_carrier_file(carrierid)
            carrier.depot = ""
            message = "Part provided by tower " + str(carrier.part_number)
        else:
            message = "Pick Part " + str(carrier.part_number) + " in location " + str(carrier.depot)
        return dict(
            message=message
            )
    message = "Carrier not found"

    return dict(
        message=message
        )


def provide_carrier_file(carrierid):
    filename = "providecarrier" + str(carrierid) + ".ETO"
    file = open(filename,"w")
    file.write("[Order]\n")
    file.write("Action=PROVIDE\n")
    file.write("Object=CARRIER\n")
    file.write("Name=" + str(carrierid)+"\n\n")
    file.close()

    samba_upload_cmd = shlex.split('smbclient //192.168.1.201/Orders -N -c "put ' + filename + '"')
    subprocess.Popen(samba_upload_cmd).wait()

