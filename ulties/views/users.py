from pyramid.compat import escape
import re
from docutils.core import publish_parts

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from ..models import User

@view_config(route_name='view_users', renderer='../templates/users.jinja2',
             permission='view')
def view_users(request):
    users = request.dbsession.query(User.name,User.role).all()
    return dict(users=users)

@view_config(route_name='edit_user', renderer='../templates/edit_user.jinja2',
             permission='edit')
def edit_user(request):
    user = request.context.user
    if 'form.submitted' in request.params:
        password = request.params['password']
        user.set_password(password)
        log.debug("Password captured = " + str(password))
        message = "Password updated " + str(user.name)
        next_url = request.route_url('view_users')
        return HTTPFound(location=next_url)
    return dict(username=user.name)
