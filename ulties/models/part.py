from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    Text,
    DateTime,
    Boolean,
    func,
    VARCHAR,
    Numeric,
    BigInteger,
    SMALLINT,
)
from sqlalchemy.orm import relationship

from sqlalchemy.dialects.postgresql import TIMESTAMP

from .meta import Base

#from urllib.parse import quote #used if you need to urlencode (percent encoding)


class Part(Base):
    """ The SQLAlchemy declarative model class for a Parts object. """
    __tablename__ = 'parts'
    id = Column(Integer, primary_key=True)
    part_number = Column(VARCHAR(length=40), nullable=False, unique=True)
    description = Column(Text)
    mpn = Column(VARCHAR(length=40))
    cost = Column(Numeric(precision=12, scale=5, asdecimal=True, decimal_return_scale=5))
    notes = Column(Text)
    created = Column(DateTime, server_default=func.now())
    modified = Column(DateTime, server_default=func.now(),
            onupdate=func.now())
    active = Column(Boolean, default=True)
    barcode_pn = Column(Text)
    barcode_spn = Column(Text)
    stock = Column(BigInteger)
    manufacturer = Column(VARCHAR(length=40))
    preferred_vendor = Column(VARCHAR(length=40))
    spn = Column(VARCHAR(length=40))
    qbpn = Column(VARCHAR(length=50))
    multiple = Column(BigInteger)
    minimum_quantity = Column(BigInteger)
    creator_id = Column(Integer)
    supply_media = Column(VARCHAR(length=1))
    reel_height = Column(SMALLINT)
    reel_diameter = Column(SMALLINT)
    
    pin1 = Column(VARCHAR(length=3))
    msl = Column(SMALLINT)
    tape_width = Column(SMALLINT)
    tape_step = Column(SMALLINT)
    tape_thickness = Column(Numeric(precision=4, scale=2, asdecimal=True, decimal_return_scale=2))
