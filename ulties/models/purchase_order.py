from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    Text,
    DateTime,
    Boolean,
    func,
    BigInteger,
    Numeric,
    VARCHAR,
    SMALLINT,
)
from sqlalchemy.orm import relationship

#from sqlalchemy.dialects.postgresql import TIMESTAMP

from .meta import Base


class Purchase_order(Base):
    """ The SQLAlchemy declarative model class for a Part_tower_article object. """
    __tablename__ = 'purchase_orders'
    id = Column(BigInteger, primary_key=True)
    po_number = Column(VARCHAR(length=40))
    user = Column(Text)
    status = Column(VARCHAR(length=1))
    supplier_name = Column(VARCHAR(length=36))
