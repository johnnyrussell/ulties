from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    Text,
    DateTime,
    Boolean,
    func,
    BigInteger,
    Numeric,
    VARCHAR,
    SMALLINT,
)
from sqlalchemy.orm import relationship

#from sqlalchemy.dialects.postgresql import TIMESTAMP

from .meta import Base


class Receive_line(Base):
    """ The SQLAlchemy declarative model class for a Part_tower_article object. """
    __tablename__ = 'receive_lines'
    id = Column(BigInteger, primary_key=True)

    po_id = Column(ForeignKey('purchase_orders.id'), nullable=True)
    poline_po = relationship('Purchase_order', backref='pos_lines')

    po_number = Column(VARCHAR(length=40))
    receive_user = Column(Text)


    receive_date = Column(DateTime, server_default=func.now())
    status = Column(VARCHAR(length=1))
    supplier_name = Column(VARCHAR(length=36))
    po_number = Column(VARCHAR(length=24))
    po_line_number = Column(Integer)
    build_id = Column(BigInteger)

    part_id = Column(ForeignKey('parts.id'), nullable=False)
    receiveline_part = relationship('Part', backref = 'received_parts')

    supplier_id = Column(Integer)
    received_quantity = Column(BigInteger)
    part_number = Column(VARCHAR(length=40), nullable=False)
    description = Column(Text)
    spn = Column(VARCHAR(length=40))
    barcode_pn = Column(Text)
    barcode_spn = Column(Text)
    piece_price = Column(Numeric(precision=12, scale=5, asdecimal=True, decimal_return_scale=5))
    status = Column(Text)
    line_number =  Column(Integer)

    receiver_id = Column(ForeignKey('users.id'), nullable=False)
    receive_creator = relationship('User', backref='created_receive')

