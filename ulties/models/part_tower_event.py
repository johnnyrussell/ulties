from sqlalchemy import (
    Column,
    BigInteger,
    VARCHAR,
    Date,
    func,
)
from sqlalchemy.orm import relationship

from sqlalchemy.dialects.postgresql import TIME

from .meta import Base


class Part_tower_event(Base):
    """ The SQLAlchemy declarative model class for a Part_tower_article object. """
    __tablename__ = 'part_tower_events'
    id = Column(BigInteger, primary_key=True)
    event_date = Column(Date, server_default=func.now())
    event_time = Column(TIME)
    event_user = Column(VARCHAR(length=15))
    event_detail = Column(VARCHAR(length=120))
