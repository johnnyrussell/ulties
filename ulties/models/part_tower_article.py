from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    Text,
    DateTime,
    Boolean,
    func,
    BigInteger,
    Numeric,
    VARCHAR,
    SMALLINT,
)
from sqlalchemy.orm import relationship

#from sqlalchemy.dialects.postgresql import TIMESTAMP

from .meta import Base


class Part_tower_article(Base):
    """ The SQLAlchemy declarative model class for a Part_tower_article object. """
    __tablename__ = 'part_tower_articles'
    id = Column(BigInteger, primary_key=True)
    created = Column(DateTime, server_default=func.now())
    create_time = Column(VARCHAR(length=8))
    create_user = Column(VARCHAR(length=15))
    article = Column(VARCHAR(length=40))
    description = Column(VARCHAR(length=40))
    cycles = Column(BigInteger)
    active = Column(Boolean, default=True)
    scan_code = Column(Text)
    stock = Column(BigInteger)
    stock_min = Column(BigInteger)
    stock_max = Column(BigInteger)
    stock_used = Column(BigInteger)
    stock_value = Column(Numeric(precision=18, scale=2, asdecimal=True, decimal_return_scale=2)) 
    stock_value_max = Column(Numeric(precision=18, scale=2, asdecimal=True, decimal_return_scale=2))
    guessed = Column(SMALLINT)
    reel_count = Column(BigInteger)
    reel_min = Column(BigInteger)
    reel_max = Column(BigInteger)
    reels_used = Column(BigInteger)
    lock = Column(VARCHAR(length=15))
    msl = Column(VARCHAR(length=2))
    msl_watch = Column(SMALLINT)
    polarized = Column(SMALLINT)
    package = Column(VARCHAR(length=20))
    package_note = Column(VARCHAR(length=20))
    part_category = Column(VARCHAR(length=15))
    custom1 = Column(VARCHAR(length=20))
    custom2 = Column(VARCHAR(length=20))
    custom3 = Column(VARCHAR(length=20))
    custom4 = Column(VARCHAR(length=20))
    custom5 = Column(VARCHAR(length=20))
    custom6 = Column(VARCHAR(length=20))

