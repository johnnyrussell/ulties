from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    DateTime,
    Boolean,
    func,
    BigInteger,
    Numeric,
    VARCHAR,
    SMALLINT,
    Date,
)
from sqlalchemy.orm import relationship

#from sqlalchemy.dialects.postgresql import SMALLINT

from .meta import Base


class Part_tower_carrier(Base):
    """ The SQLAlchemy declarative model class for a Part_tower_article object. """
    __tablename__ = 'part_tower_carriers'
    carrier = Column(BigInteger, primary_key=True)
    created = Column(DateTime, server_default=func.now())
    create_time = Column(VARCHAR(length=8))
    create_user = Column(VARCHAR(length=15))
    article = Column(VARCHAR(length=40))
    depot = Column(VARCHAR(length=40))
    depot_date = Column(Date)
    depot_old = Column(VARCHAR(length=40))
    depot_time = Column(VARCHAR(length=8))
    depot_user = Column(VARCHAR(length=15))
    stock = Column(BigInteger)
    stock_min = Column(BigInteger)
    stock_new = Column(BigInteger)
    stock_used = Column(BigInteger)
    stock_tpssys = Column(BigInteger)
    out_time = Column(BigInteger)
    guessed = Column(SMALLINT)
    unleaded = Column(SMALLINT)
    price_part = Column(Numeric(precision=18, scale=3, asdecimal=True, decimal_return_scale=3)) 
    cycles = Column(BigInteger)
    height = Column(SMALLINT)
    h_code = Column(SMALLINT)
    diameter = Column(SMALLINT)
    supply = Column(VARCHAR(length=1)) #, comment='Media (R=Reel,T=Tray,etc.)'))
    traybox = Column(VARCHAR(length=15))
    pin1 = Column(VARCHAR(length=3))
    magazine = Column(VARCHAR(length=15))
    feeder = Column(VARCHAR(length=15))
    step_width = Column(SMALLINT)
    duration = Column(BigInteger)
    frequency = Column(BigInteger)
    amplitude = Column(BigInteger)
    z_acc = Column(VARCHAR(length=50))
    y_acc = Column(VARCHAR(length=50))
    lock = Column(VARCHAR(length=15))
    msl = Column(VARCHAR(length=2))
    msl_watch = Column(SMALLINT)
    msl_date = Column(DateTime)
    msl_time =Column(VARCHAR(length=8))
    dry_date = Column(Date)
    dry_time = Column(VARCHAR(length=8))
    expiry = Column(Date)
    core = Column(Integer)
    tape_height = Column(SMALLINT)
    custom_order = Column(VARCHAR(length=15))
    manufacturer = Column(VARCHAR(length=20))
    manufacturer_reference = Column(VARCHAR(length=30))
    manufacturer_barcode = Column(VARCHAR(length=15))
    batch = Column(VARCHAR(length=25))
    custom1 = Column(VARCHAR(length=20))
    custom2 = Column(VARCHAR(length=20))
    custom3 = Column(VARCHAR(length=20))
    custom4 = Column(VARCHAR(length=20))
    custom5 = Column(VARCHAR(length=20))
    custom6 = Column(VARCHAR(length=20))

