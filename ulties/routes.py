from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPFound,
)
from pyramid.security import (
    Allow,
    Everyone,
)

#import logging
#log = logging.getLogger(__name__)

from .models import Page
from .models import Purchase_order
from .models import Part_inventory
from .models import User

def search_carrier_pregen(request, elements, kw):
    kw.setdefault('message', '')
    return elements, kw


def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('search_barcode', '/')
#    config.add_route('search_barcode', '/createcarrier')
    config.add_route('view_wiki', '/wiki') 
    config.add_route('search_po', '/checkin')
    config.add_route('view_users', '/users', factory=users_page_factory)
    config.add_route('edit_user', '/edituser/{username}', factory=edit_user_factory)
#    config.add_route('search_carrier', '/carrier')
    config.add_route('search_carrier', '/searchcarrier/{message:.*}', pregenerator=search_carrier_pregen)
    config.add_route('update_carrier', '/carrier/{carrierid}', factory=carrier_factory)
    config.add_route('create_carrier', '/createcarrier/{barcodepn:.*}', factory=new_carrier_factory)
    config.add_route('provide_part', '/providepart/{carrierid}', factory=carrier_factory)
    config.add_route('recent_carriers', '/recent')
    config.add_route('reprint_carrier', '/reprint')
    config.add_route('search_part_number', '/searchpart')
    config.add_route('create_part_data', '/createpart/{partnumber:.*}', factory=create_part_factory)
    config.add_route('edit_part_data', '/part/{partnumber:.*}', factory=edit_part_factory)
    config.add_route('receive_part', '/receivepo/{ponumber}', factory=receive_part_factory)
    config.add_route('pick_parts', '/pickparts')
    config.add_route('pick_depots', '/pickdepots/{partnumber}', factory=edit_part_factory)
    config.add_route('special_operations', '/specialops', factory=specialops_factory)
    config.add_route('login', '/login')
    config.add_route('logout', '/logout')
    config.add_route('view_page', '/{pagename}', factory=page_factory)
    config.add_route('add_page', '/add_page/{pagename}',
                     factory=new_page_factory)
    config.add_route('edit_page', '/{pagename}/edit_page',
                     factory=page_factory)

def users_page_factory(request):
    return ViewUsers()

class ViewUsers(object):
    def __init__(self):
        pass

    def __acl__(self):
        return [
            (Allow, 'role:editor', 'view'),
            (Allow, 'role:basic', 'view'),
        ]

def specialops_factory(request):
    return SpecialOps()

class SpecialOps(object):
    def __init__(self):
        pass

    def __acl__(self):
        return [
            (Allow, 'role:editor', 'view'),
        ]


def edit_user_factory(request):
    username = request.matchdict['username']
#    log.debug("username = " + str(username))
    user = request.dbsession.query(User).filter_by(name=username).first()
    if user is None:
        raise HTTPNotFound
    return UserEdit(user)

class UserEdit(object):
    def __init__(self,user):
        self.user = user

    def __acl__(self):
        return [
            (Allow, 'role:editor', 'edit'),
        ]

def new_page_factory(request):
    pagename = request.matchdict['pagename']
    if request.dbsession.query(Page).filter_by(name=pagename).count() > 0:
        next_url = request.route_url('edit_page', pagename=pagename)
        raise HTTPFound(location=next_url)
    return NewPage(pagename)

class NewPage(object):
    def __init__(self, pagename):
        self.pagename = pagename

    def __acl__(self):
        return [
            (Allow, 'role:editor', 'create'),
            (Allow, 'role:basic', 'create'),
        ]

def page_factory(request):
    pagename = request.matchdict['pagename']
    page = request.dbsession.query(Page).filter_by(name=pagename).first()
    if page is None:
        raise HTTPNotFound
    return PageResource(page)

class PageResource(object):
    def __init__(self, page):
        self.page = page

    def __acl__(self):
        return [
            (Allow, Everyone, 'view'),
            (Allow, 'role:editor', 'edit'),
            (Allow, str(self.page.creator_id), 'edit'),
        ]

# this factory checks for a matching po number 

def receive_part_factory(request):
    ponumber = request.matchdict['ponumber']

    if request.dbsession.query(Purchase_order).filter_by(po_number=ponumber).count() > 0:
        return NewRec(ponumber)
    next_url = request.route_url('search_po')
    raise HTTPFound(location=next_url)
    return NewRec(ponumber)

class NewRec(object):
    def __init__(self, ponumber):
        self.ponumber = ponumber

    def __acl__(self):
        return [
            (Allow, 'role:editor', 'create'),
            (Allow, 'role:basic', 'create'),
        ]

def new_carrier_factory(request):
    barcodepn = request.matchdict['barcodepn']
    return NewCarrier(barcodepn)

class NewCarrier(object):
    def __init__(self, barcodepn):
        self.barcodepn = barcodepn

    def __acl__(self):
        return [
            (Allow, 'role:editor', 'create'),
            (Allow, 'role:basic', 'create'),
        ]


def create_part_factory(request):
    partnumber = request.matchdict['partnumber']
    return NewPart(partnumber)

class NewPart(object):
    def __init__(self, partnumber):
        self.partnumber = partnumber

    def __acl__(self):
        return [
            (Allow, 'role:editor', 'create'),
            (Allow, 'role:basic', 'create'),
        ]

def carrier_factory(request):
    carrierid = request.matchdict['carrierid']
    return UpdateCarrier(carrierid)

class UpdateCarrier(object):
    def __init__(self, carrierid):
        self.carrierid = carrierid

    def __acl__(self):
        return [
            (Allow, 'role:editor', 'edit'),
            (Allow, 'role:basic', 'edit'),
        ]

def edit_part_factory(request):
    partnumber = request.matchdict['partnumber']
    return EditPart(partnumber)

class EditPart(object):
    def __init__(self, partnumber):
        self.partnumber = partnumber

    def __acl__(self):
        return [
            (Allow, 'role:editor', 'edit'),
            (Allow, 'role:basic', 'edit'),
        ]

#def search_carrier_pregen(request, elements, kw):
#    kw.setdefault('message', '')
#    return elements, kw

#def search_carrier_factory(request):
#    if request.matchdict['message']:
#        message = request.matchdict['message']
#    else:
#        message = ""
#    return SearchCarrierResource(message)

#class SearchCarrierResource(object):
#    def __init__(self, message):
#        self.message = message

#    def __acl__(self):
#        return [
#            (Allow, Everyone, 'view'),
#        ]

