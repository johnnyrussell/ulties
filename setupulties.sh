#!/bin/bash
#Mostly based on examples at:
# https://docs.pylonsproject.org/projects/pyramid/en/latest/tutorials/wiki2/index.html
# https://avolkov.github.io/pyramid-web-app-setup-with-with-postgres-sqlalchemy-and-alembic.html
# useful auth demo https://michael.merickel.org/projects/pyramid_auth_demo/
# navigation bar https://realpython.com/blog/python/primer-on-jinja-templating/

# Depends on postgresql database with database ulties user ulties hardcoded for now
# for label printing. sudo apt-get install glabels

# using setcap to dirty open host port 80 # sudo apt-get install libcap2-bin 
# sudo setcap 'cap_net_bind_service=+ep' /path/to/program
# pointed to python3


#Requires python 3.5? python3-venv postgresql smbclient cups


mkdir ~/ulties
mkdir ~/ulties/env
export VENV=~/ulties/env
python3 -m venv $VENV
$VENV/bin/pip install --upgrade pip
$VENV/bin/pip install "pyramid==1.9.1"
$VENV/bin/pip install simple_zpl2
$VENV/bin/pip install webtest pytest pytest-cov deform sqlalchemy \
  pyramid_chameleon pyramid_debugtoolbar waitress \
  pyramid_tm zope.sqlalchemy pyramid_bootstrap

#wiki2

#do not need cookiecutter if cloning from repo
#$VENV/bin/pip install cookiecutter
cd ~/ulties
#$VENV/bin/cookiecutter --no-input gh:Pylons/pyramid-cookiecutter-alchemy --checkout 1.9-branch project_name=ulties repo_name=ulties
#mv ulties temp
#mv temp/* .
#rm -rf ulties


$VENV/bin/pip install -e ".[testing]"

#postgres user ulties pass ulties

#these steps are initializing alembric and may not be necessary on cloned builds
#intitialized alembic folders
#$VENV/bin/alembic init alembic

#Set database address ini development.ini and alembic.ini, adde alembic requirement to setup.py

#Configured alembic to manage migrations from https://avolkov.github.io/pyramid-web-app-setup-with-with-postgres-sqlalchemy-and-alembic.html
#then generate migrations revision
#$VENV/bin/alembic revision --autogenerate -m "Initial Commit"


#Updates database to version defined in models
#$VENV/bin/alembic upgrade head

#Populate default data for initial install. Migration/updates use alembic
#$VENV/bin/initialize_ulties_db development.ini

#Running the tests


#Start application production
#$VENV/bin/pserve development.ini --reload
$VENV/bin/pserve production.ini --reload

