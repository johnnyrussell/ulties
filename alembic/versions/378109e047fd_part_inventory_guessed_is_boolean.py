"""part_inventory.guessed is boolean

Revision ID: 378109e047fd
Revises: 6a05cd0149b1
Create Date: 2018-03-29 17:08:06.315275

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '378109e047fd'
down_revision = '6a05cd0149b1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('part_inventories', 'guessed',
               existing_type=sa.SMALLINT(),
               type_=sa.Boolean(),
               existing_nullable=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('part_inventories', 'guessed',
               existing_type=sa.Boolean(),
               type_=sa.SMALLINT(),
               existing_nullable=True)
    # ### end Alembic commands ###
