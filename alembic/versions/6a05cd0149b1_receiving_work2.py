"""receiving work2

Revision ID: 6a05cd0149b1
Revises: 35c6eee740b1
Create Date: 2018-03-15 23:02:30.275274

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6a05cd0149b1'
down_revision = '35c6eee740b1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('uq_receive_lines_part_number', 'receive_lines', type_='unique')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_unique_constraint('uq_receive_lines_part_number', 'receive_lines', ['part_number'])
    # ### end Alembic commands ###
