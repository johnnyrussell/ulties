"""scancode -> barcode_pn

Revision ID: f22fa50622ef
Revises: f959b3707a95
Create Date: 2018-03-12 12:08:45.509804

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f22fa50622ef'
down_revision = 'f959b3707a95'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('parts', sa.Column('barcode_pn', sa.Text(), nullable=True))
    op.drop_column('parts', 'scan_code')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('parts', sa.Column('scan_code', sa.TEXT(), autoincrement=False, nullable=True))
    op.drop_column('parts', 'barcode_pn')
    # ### end Alembic commands ###
